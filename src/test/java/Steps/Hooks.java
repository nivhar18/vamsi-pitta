package Steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import week7.day1.SeMethods;

public class Hooks extends SeMethods{
@Before
public void beforeScenario(Scenario sc)
{
	beginResult();
	testCaseName=sc.getName();
	testCaseDesc=sc.getId();
	category="smoke";
	author="nivedha";
	startTestCase();
	startApp("chrome","http://leaftaps.com/opentaps");
	System.out.println(sc.getName());
	System.out.println(sc.getId());
}
@After
public void afterScenario(Scenario sc)
{
	System.out.println(sc.getStatus());
	System.out.println(sc.isFailed());
	closeBrowser();
	endResult();
}


}
