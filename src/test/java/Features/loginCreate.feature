Feature: Login and createLead for LeafTaps
#Background:
#Given Open the chrome Browser 
#And maximize the browser
#And Set TimeOuts
#And Hit the URL

@Positive
Scenario Outline: Positive create lead flow

And Enter the UserName as <un>
And Enter the Password as <pwd>
When click on the login button
#Then verify login is success
When click on crmsfa
When click on create lead 
And Enter the company name as <comp> 
And Enter the first name as <fn> 
And Enter the last name as <ln> 
When click on create button
#Then verify Lead creation is success
And Verify first name as <vname>

Examples:
|un|pwd|comp|fn|ln|
|DemoCSR|crmsfa|TCS|Niv|Hari|

#@Negative
#Scenario Outline: Positive create lead flow
#
#And Enter the UserName as <un>
#And Enter the Password as <pwd>
#When click on the login button
#Then verify login is success
#When click on crmsfa
#When click on create lead 
#And Enter the company name as <comp>
#And Enter the first name as <fn>
#And Enter the last name as <ln>
#When click on create button
#Then verify Lead creation is success
#And Verify first name as <vname>
#
#Examples:
#|un|pwd|comp|fn|ln|vname|
#|DemoCSR|crmsfa1|TCS|Niv|Hari|Demo B2B CSR|

