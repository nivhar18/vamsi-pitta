package week1.day2;

import java.util.Scanner;

public class SecMaxvalueinArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=sc.nextInt();
		int a[]=new int[n];
		System.out.println("Enter array elements:");
		for (int i=0; i<a.length; i++) 
		{
			a[i] = sc.nextInt();
		}
		int large=a[0];
		int seclarge=0;
		for(int i=1;i<a.length;i++)
		{
			if(a[i]>large)
			{
				seclarge=large;
				large=a[i];
			}
			else if(a[i]>seclarge)
			{
				seclarge=a[i];
			}
		}
		System.out.println("Max element in the array: "+seclarge);
	}

}
