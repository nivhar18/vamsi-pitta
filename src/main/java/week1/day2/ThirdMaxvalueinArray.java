package week1.day2;

import java.util.Scanner;

public class ThirdMaxvalueinArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=sc.nextInt();
		int a[]=new int[n];
		System.out.println("Enter array elements:");
		for (int i=0; i<a.length; i++) 
		{
			a[i] = sc.nextInt();
		}
		
		int first = a[0];
		for (int i = 1;i < a.length ; i++)
			if (a[i] > first)
				first = a[i];

		// Find second 
		// largest element
		int second = Integer.MIN_VALUE;
		for (int i = 0;i < a.length ; i++)
			if (a[i] > second && a[i] < first)
				second = a[i];

		// Find third
		// largest element
		int third = Integer.MIN_VALUE;
		for (int i = 0;i < a.length ; i++)
			if (a[i] > third && a[i] < second)
				third = a[i];

		System.out.println("The third Largest element is "+third);
		
	}

}
