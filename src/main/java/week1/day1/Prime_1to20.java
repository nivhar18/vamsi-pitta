package week1.day1;

import java.util.Scanner;

public class Prime_1to20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.println("Enter a range to find prime number:");
		int num=s.nextInt();
		int count;

		  for (int i = 1; i<= num; i++) {
		  count = 0;
		   for (int j = 2; j <= i / 2; j++) {
			  
		    if (i % j == 0) {
		     count++;
		     break;
		    }
		   }

		   if (count == 0) {
		    System.out.println(i);
		   }

		  }
	}

}
