package week1.day1;

import java.util.Scanner;

public class Sum_Oddnum_50to100 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("The odd numbers from 50 to 100 are:");
		int sum=0;
		for(int i=50;i<=100;i++)
		{
			if(i%2!=0)
			{
				sum=sum+i;				
			}			
		}
		System.out.println(sum);
	}
}