package week1.day1;

import java.util.Scanner;

public class SumOfOddDigit_in_num {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number to find the sum of all odd & even digits");
	    int n = sc.nextInt();
	    int mod; //Modulus
        int od = 0, even = 0;
	    for(int i = 0; i <=n ; i++) {
            //Separates each digit from given number 
            mod = n % 10;  
            n = n / 10;

            if(mod % 2 == 0) { //Determines if the number is odd or even
                even = even + mod; //Addition of even numbers
            } else {
                od = od + mod; //Addition of odd numbers
            } 
        }
        System.out.println("Sum of even numbers is "+even);
        System.out.println("Sum of odd numbers is " + od);
    }  
	    
	}


