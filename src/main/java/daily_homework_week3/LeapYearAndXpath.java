package daily_homework_week3;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LeapYearAndXpath {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a year to find leap or not: ");
		int year=sc.nextInt();
		if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
		{
			System.out.println(year+" is a leap year");
		}
		else
		{
			System.out.println(year+" is not a leap year");
		}
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/Dropdown.html");
		
		WebElement b=d.findElementByXPath("(//select/option[@value='3'])[2]");
		//in html index starts from 1,hence selecting 2nd dropdown with last but one option
		b.click();
		
		d.get("http://leafground.com/pages/checkbox.html");
		List<WebElement> c=d.findElementsByXPath("//input[@type='checkbox']");
		int checkcount=0,uncheckcount=0;
		for(int i=0;i<c.size();i++)
		{
			if(c.get(i).isSelected()==true)
			{
				checkcount++;
			}
			else
			{
				uncheckcount++;
			}
		}
		
		System.out.println("Number of Checked boxes: "+checkcount+"\n"+
		"Number of UnChecked boxes: "+uncheckcount);
	}

}
