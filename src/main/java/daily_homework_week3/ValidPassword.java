package daily_homework_week3;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ValidPassword {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the password: ");
		String s=sc.next();
		String pat="((?=.*\\d{2})(?=.*[a-z]{2})(?=.*[A-Z]{1})(?=.*[@#$%]).{6,10})";
		Pattern p=Pattern.compile(pat);
		Matcher m=p.matcher(s);
		System.out.println(m.matches());
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://testleaf.herokuapp.com/pages/checkbox.html");
		List<WebElement> c=d.findElementsByXPath("//input[@type='checkbox']");
		for(int i=0;i<c.size();i++)
		{
			if(c.get(i).isSelected()==false)
			{
				c.get(i).click();
			}
		
		}
		
	}

}
