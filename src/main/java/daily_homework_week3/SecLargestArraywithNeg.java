package daily_homework_week3;

import java.util.Scanner;

public class SecLargestArraywithNeg {

	public static void main(String[] args) {
		int n, temp = 0;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the range/size of elements: ");
		n = s.nextInt();
		int a[] = new int[n];
		System.out.println("Enter the elements:");
		for (int i = 0; i < n; i++) 
		{
			a[i] = s.nextInt();
		}

		//sorting elements
		for (int i = 0; i < n; i++) 
		{
			for (int j = i + 1; j < n; j++) 
			{
				if (a[i] > a[j]) 
				{
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}

		System.out.println("Second largest value is: "+a[n-2]);	
	
		/* eg:To find 2nd max among,23,-200,44,22,11,12  in this case n=6
		     -200,11,12,22,23,44
		 n=   0   1  2  3  4  5   
		 so a[n-2]=a[4]=23 */

	}   
}