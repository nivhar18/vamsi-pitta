package daily_homework_week3;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

public class RepeatNumArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = {1,2,2,3,4,5,4,7,6,7,8,8};
        
        Set<Integer> set = new HashSet<Integer>(); 
        System.out.println("Duplicated numbers are : ");
        for(int i = 0; i < array.length ; i++)
        {
            //If same integer is already present then add method will return FALSE
        	// if(!set.add(array[i]))
            if(set.add(array[i]) == false)
            {
                    System.out.println(array[i]);
            }
           
        }
        
	}

}
