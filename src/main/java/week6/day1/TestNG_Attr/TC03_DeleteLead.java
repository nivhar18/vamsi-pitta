package week6.day1.TestNG_Attr;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TC03_DeleteLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC03_DeleteLead";
		testCaseDesc="Delete an existing lead";
		category="smoke";
		author="nivedha";
		
	}
	@Test(dependsOnMethods=("week6.day1.TC01_CreateLead.createLead"))
	//if createlead fails then delete lead will be skipped
	public void deleteLead() throws InterruptedException {
		
		WebElement lead = locateElement("linktext", "Leads");
		click(lead);
		WebElement fleadlink = locateElement("xpath", "//a[text()='Find Leads']");
		click(fleadlink);
		WebElement phone = locateElement("xpath", "//span[text()='Phone']");
		click(phone);// 91 044-9548565653
		WebElement ccode = locateElement("name", "phoneCountryCode");
		clearElement(ccode);
		type(ccode, "91");
		WebElement acode = locateElement("name", "phoneAreaCode");
		type(acode, "044");
		WebElement phn = locateElement("name", "phoneNumber");
		type(phn, "9548565653");
		WebElement fleadbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(fleadbutton);
		Thread.sleep(1000);	
		WebElement firstleadid = locateElement("xpath", "(//tbody/tr/td/div/a)[1]");
		getText(firstleadid);
		click(firstleadid);
		Thread.sleep(1000);
		WebElement del = locateElement("xpath", "//a[text()='Delete']");
		click(del);
		Thread.sleep(1000);
		WebElement fleadlink1 = locateElement("xpath", "//a[text()='Find Leads']");
		click(fleadlink1);
		WebElement leadid = locateElement("xpath", "(//div/input)[33]");
		type(leadid, "11670");		
		WebElement fleadbutton1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(fleadbutton1);
		WebElement error = locateElement("xpath", "//div[text()='No records to display']");
		getText(error);
		
	}
}
