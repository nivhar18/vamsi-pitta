package week6.day1.TestNG_para;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods{
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}
	@Parameters({"url","un","pwd"})
	@BeforeMethod
	public void login(String url,String un,String pwd) {
		startApp("chrome",url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, un);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linktext","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	 
}
