package week6.day1.TestNG_para;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TC01_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC01_CreateLead";
		testCaseDesc="Create a new lead";
		category="smoke";
		author="nivedha";
		
	}
	@Test(dataProvider="positive")
	public void createLead(String cname,String fn,String ln) throws InterruptedException
	{
		
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);	
		WebElement compname =locateElement("id","createLeadForm_companyName");
		type(compname, cname);
		WebElement firstname =locateElement("id","createLeadForm_firstName");
		type(firstname, fn);		
		WebElement lastname =locateElement("id","createLeadForm_lastName");
		type(lastname, ln);
		WebElement create =locateElement("xpath","(//input[@value='Create Lead'])[1]");
		click(create);
		
	}
	
	@DataProvider(name="positive")
	public Object[][] fetchData()
	{
		 Object[][] data=new Object[2][3];
		 data[0][0]="Testleaf";
		 data[0][1]="Niv";
		 data[0][2]="Har";
		 
		 data[1][0]="TCS";
		 data[1][1]="joli";
		 data[1][2]="pat";
		 
	 return data;
		
	}
	
	@DataProvider(name="negative")
	public Object[][] fetchNegData()
	{	return null;}


	}
