package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TC01_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC01_CreateLead";
		testCaseDesc="Create a new lead";
		category="smoke";
		author="nivedha";
		excelfilename="note";
		
	}
	@Test(dataProvider="fetchData")
	public void createLead(String cname,String fn,String ln,String salute,String phn) throws InterruptedException
	{
		
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);	
		WebElement compname =locateElement("id","createLeadForm_companyName");
		type(compname, cname);
		WebElement firstname =locateElement("id","createLeadForm_firstName");
		type(firstname, fn);		
		WebElement lastname =locateElement("id","createLeadForm_lastName");
		type(lastname, ln);
		WebElement salute1 =locateElement("name","personalTitle");
		type(salute1,salute);
		WebElement phn1 =locateElement("xpath","(//input[@class='inputBox'])[17]");
		type(phn1,phn);
		
	}



}
