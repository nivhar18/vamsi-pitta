package week6.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TC02_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC02_EditLead";
		testCaseDesc="Edit an existing lead";
		category="smoke";
		author="nivedha";
		excelfilename="edit";
		
	}
	@Test
	public void editLead() throws InterruptedException
	{
	
		WebElement lead =locateElement("linktext","Leads");
		click(lead);			
		WebElement fleadlink =locateElement("xpath","//a[text()='Find Leads']");
		click(fleadlink);
		WebElement firstname =locateElement("xpath","(//input[@name='firstName'])[3]");
		type(firstname, "nivedha");
		WebElement fleadbutton =locateElement("xpath","//button[text()='Find Leads']");
		click(fleadbutton);
		Thread.sleep(1000);
		WebElement firstleadid =locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstleadid);
		Thread.sleep(1000);
		verifyTitle("View Lead | opentaps CRM");
		WebElement edit =locateElement("linktext","Edit");
		click(edit);
		Thread.sleep(1000);
		WebElement compname =locateElement("id","updateLeadForm_companyName");
		clearElement(compname);
	    type(compname,"Tata Consultancy Services");
	    WebElement update =locateElement("xpath","//input[@value='Update']");
		click(update);
		Thread.sleep(1000);
		WebElement verifycn =locateElement("id","viewLead_companyName_sp");
		verifyPartialText(verifycn,"Tata");
		
	}
}
