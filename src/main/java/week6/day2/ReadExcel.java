package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename) throws IOException {
	XSSFWorkbook wbook=new XSSFWorkbook("./datasetup/"+filename+".xlsx");
	//save as excel workbook
	XSSFSheet sheet=wbook.getSheetAt(0);
	//get sheet of index 0
	int rowCount=sheet.getLastRowNum();
	//finding row count by getting last row num in sheet
	System.out.println("Row count in sheet "+rowCount);
	int colCount=sheet.getRow(0).getLastCellNum();
	//finding col count by getting 1st row-last cell
	System.out.println("Column count in sheet "+colCount);
	Object[][] data=new Object[rowCount][colCount];
	System.out.println("The values are: ");
	for(int j=1;j<=rowCount;j++) //data is from 2nd row
	{
		XSSFRow row=sheet.getRow(j);//get value of row j
		for(int i=0;i<colCount;i++) //column header from row 0
		{
			XSSFCell cell=row.getCell(i); //point to cell at row i
			data[j-1][i]=cell.getStringCellValue(); //get value of cell as string
			System.out.println(data);
		}
	}
	return data;
	
	}

}
