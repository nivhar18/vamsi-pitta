package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;

public class WindowandSnapshot {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("https://www.irctc.co.in/nget/train-search");//window 1
		d.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();//opens at window 1
		d.findElementByLinkText("Contact Us").click();//window 2
		Set<String> allwin = d.getWindowHandles();//has two references 0 and 1
		List<String> listwin=new ArrayList<>();
		listwin.addAll(allwin);
		d.switchTo().window(listwin.get(1));//go to window 2
		System.out.println("Title: "+d.getTitle()+"\n"+"URL: "+d.getCurrentUrl());//title and url of window 2
		d.manage().window().maximize();//maximize window 2
		File src=d.getScreenshotAs(OutputType.FILE);//screenshot of window 2
		File dest=new File("./screenshot/contact.png");
		FileUtils.copyFile(src, dest);
		d.switchTo().window(listwin.get(0));//switch to window 1
		d.close();//close window 1		
		//System.out.println(d.getPageSource().contains("10"));
	}

}
