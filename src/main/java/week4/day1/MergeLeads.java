package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://leaftaps.com/opentaps");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click();
		d.findElementByLinkText("Leads").click();
		
		d.findElementByLinkText("Merge Leads").click();
		d.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> allwin = d.getWindowHandles();//has two references 0 and 1
		List<String> listwin=new ArrayList<>();
		listwin.addAll(allwin);
		d.switchTo().window(listwin.get(1));//go to window 2
		d.manage().window().maximize();
		d.findElementByXPath("//input[@id='ext-gen25']").sendKeys("102");
		d.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		d.findElementByXPath("(//tbody/tr/td/div/a)[1]").click();
		d.switchTo().window(listwin.get(0));
		d.findElementByLinkText("Merge").click();
		Alert pa=d.switchTo().alert();
		String txt=pa.getText();
		System.out.println("Message in prompt box= "+txt);
		pa.accept();	
		//d.findElementByXPath("//button[text()='Find Leads']").click();
		WebElement b = d.findElementByXPath("//li[@class='errorMessage']");
		System.out.println("Verifying Error Message : "+b.getText());		
		d.findElementByXPath("(//tbody/tr/td/input)[1]").sendKeys("10256");
		//d.findElementByXPath("//button[text()='Find Leads']").click();
		d.close();
	}

}
