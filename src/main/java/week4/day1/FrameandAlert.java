package week4.day1;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FrameandAlert {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		d.switchTo().frame("iframeResult"); //using iframe id
		WebElement a=d.findElementByXPath("//button[text()='Try it']"); //accessing button text using xpath
		a.click();
		Alert pa=d.switchTo().alert();
		String txt=pa.getText();
		System.out.println("Message in prompt box= "+txt);
		pa.sendKeys("Nivedha");
		Thread.sleep(1000);
		pa.accept();
		WebElement b = d.findElementByXPath("//p[@id='demo']");
		System.out.println("Message inside frame: "+b.getText());
		//pa.dismiss();
		//WebElement b = d.findElementByXPath("//p[@id='demo']");
		//System.out.println(b.getText());
		d.switchTo().defaultContent();		
	}

}
