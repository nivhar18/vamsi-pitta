package week4.day1;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DragDropCoordinate {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://jqueryui.com/droppable/");
		Actions builder=new Actions(d);
//		WebElement frame=d.findElementByClassName("demo-frame");//full frame
//		d.switchTo().frame(frame);
//		WebElement drag=d.findElementById("draggable");//drag box id
//		int x=drag.getLocation().getX();
//		int y=drag.getLocation().getY();		
//		builder.dragAndDropBy(drag, x+30, y+50).perform();
		
		Thread.sleep(1000);
		d.findElementByLinkText("Sortable").click();
		WebElement frame2=d.findElementByClassName("demo-frame");
		d.switchTo().frame(frame2);
		WebElement dg=d.findElementByXPath("(//li[@class='ui-state-default ui-sortable-handle'])[1]");
		WebElement dp=d.findElementByXPath("(//li[@class='ui-state-default ui-sortable-handle'])[4]");
		builder.clickAndHold(dg).moveToElement(dp).release(dg).dragAndDrop(dg, dp).build().perform();
		Thread.sleep(3000);
		d.close();
	}

}
