package week2.day2;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.Put;

public class Map_FindDupChar {
	public static void main(String[] args) {
		String name="Nivedhaaaa";
		char[] ch=name.toCharArray();

		Map<Character, Integer> m=new HashMap<Character, Integer>();
		for (char c : ch) 
		{
			if(m.containsKey(c))
			{
				Integer val=m.get(c)+1;
				m.put(c,val);
				if(m.put(c,val)>0)
				{
			System.out.println(c);
				}
				
			}	
			else
			{
				m.put(c, 1);				
			}			
		}
		System.out.println(m);
	}

}
