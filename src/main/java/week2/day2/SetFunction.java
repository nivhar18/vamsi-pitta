package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class SetFunction {

	public static void main(String[] args) {
		
		//Set<String> m=new HashSet<String>();
		//Set<String> t=new TreeSet<String>();
		Set<String> lh=new LinkedHashSet<String>();//to maintain order of mobiles
		lh.add("nokia");
		lh.add("sam");
		lh.add("moto");
		lh.add("sam");
		System.out.println("The mobiles are:");
		for (String es : lh) {
			System.out.println(es);
			
		}
		List<String> lst=new ArrayList<String>();
		lst.addAll(lh);
		//int size=lh.size();
		String str=lst.get(0);
		System.out.println("The first used mobile is: "+str);

	}

}
