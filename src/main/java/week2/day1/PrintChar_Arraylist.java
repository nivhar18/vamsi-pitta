package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class PrintChar_Arraylist {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.print("Enter a option: "+"\n"+"1.Sorting 2.Reverse sorting in lowercase 3.Removing duplicates");
		int option = sc.nextInt();

		switch (option) {
		case 1:
		{
			String str="Amazon India";
			List<Character> mp=new ArrayList<Character>();
			char ch[]=str.toCharArray();
			for (char es : ch) {
				mp.add(es);
			}

			System.out.println("The sorted order characters in ASCII: ");
			Collections.sort(mp);	
			for (char ec : mp) {
				System.out.println(ec);
			}
		}
		break;
		case 2:
		{
			String str="Amazon India";
			List<Character> mp=new ArrayList<Character>();
			String low= str.toLowerCase();
			char[] chlow=low.toCharArray();
			for (char elow : chlow) {
				mp.add(elow);
			}

			System.out.println("The reverse order characters in ASCII: ");
			Collections.reverse(mp);	
			for (char er : mp) {
				System.out.println(er);
			}
		}
		break;
		case 3:
		{
			String str="Amazon India";
			List<Character> mp=new ArrayList<Character>();
			String low= str.toLowerCase();
			char[] chlow=low.toCharArray();
			for (char elow : chlow) {
				mp.add(elow);
			}
			for(int i=0;i<mp.size();i++)
			{
				for(int j=i+1;i<mp.size();j++)
				{
					char a=mp.get(i);
					char b=mp.get(j);
					if(a==b)
					{
						mp.remove(b);
					}
				}
			}

			System.out.println("The sorted characters after removing duplicates: ");
			Collections.sort(mp);	
			for (char er : mp) {
				System.out.println(er);
			}
		}

		break;
		default:
			System.out.println("Invalid option.Choose an option from 1 to 3");
			break;

		}

	}

}
