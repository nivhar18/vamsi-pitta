package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.bytebuddy.implementation.bytecode.ByteCodeAppender.Size;

public class ListFunctions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> mp=new ArrayList<String>();
		mp.add("Nokia");
		mp.add("iphone");
		mp.add("samsung");
		mp.add("Oneplus");
		mp.add("Samsung");

		System.out.println("The mobiles are: ");
		for (String es : mp) {
			System.out.println(es);
		}
		int size=mp.size();
		System.out.println("The count of mobiles: "+size);
		String s1=mp.get(size-1);
		System.out.println("The last mobile in list is "+s1);

		System.out.println("The sorted order of mobiles in ASCII: ");
		Collections.sort(mp);	
		for (String es : mp) {
			System.out.println(es);
		}
		System.out.println("The reverse order of mobiles in ASCII: ");
		Collections.reverse(mp);	
		for (String es : mp) {
			System.out.println(es);
		}

	}

}
