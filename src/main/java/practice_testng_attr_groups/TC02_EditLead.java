package practice_testng_attr_groups;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TC02_EditLead extends ProjectMethods{
	@BeforeTest(groups= {"sanity"},dependsOnGroups= {"smoke"})
	public void setData()
	{
		testCaseName="TC02_EditLead";
		testCaseDesc="Edit an existing lead";
		category="smoke";
		author="nivedha";
		
	}
	@Test(groups= {"sanity"},dependsOnGroups= {"smoke"},alwaysRun=true)
	public void editLead() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
			ChromeDriver d=new ChromeDriver();	
			d.manage().window().maximize();
			d.get("https://leafground.com/checkbox.xhtml");
				List<WebElement> chkbox=d.findElementsByXPath("//span[@class='ui-chkbox-icon ui-icon ui-icon-blank ui-c']");
				int s1=chkbox.size();
				System.out.println("Total num of chkboxes in page: "+s1);
				d.findElementByXPath("(//span[@class='ui-chkbox-icon ui-icon ui-icon-blank ui-c'])[7]/parent::div").click();	
				WebElement toggle=d.findElementByXPath("//div[@class='ui-toggleswitch-slider']/parent::div");
				toggle.click();
		d.close();
	}
}
