package daily_homework_week1;

import java.util.Scanner;

public class SortAscDescArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n, temp;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the range/size of elements: ");
		n = s.nextInt();
		int a[] = new int[n];
		System.out.println("Enter the elements:");
		for (int i = 0; i < n; i++) 
		{
			a[i] = s.nextInt();
		}

		//sorting elements
		for (int i = 0; i < n; i++) 
		{
			for (int j = i + 1; j < n; j++) 
			{
				if (a[i] > a[j]) 
				{
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}

		//print sorted elements 		
		System.out.println("Ascending Order:");
		for (int i = 0; i < n ; i++) 
		{
			System.out.println(a[i]);
		}
		for (int i = 0; i < n; i++) 
		{
			for (int j = i + 1; j < n; j++) 
			{
				if (a[i] < a[j]) 
				{
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}

		System.out.println("Descending Order:");
		for (int i = 0; i < n ; i++) 
		{
			System.out.println(a[i]);
		}

	}   
}