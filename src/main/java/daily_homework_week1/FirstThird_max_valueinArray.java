package daily_homework_week1;

import java.util.Scanner;

public class FirstThird_max_valueinArray {

	public static void main(String[] args) {
		int n, temp = 0;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the range/size of elements: ");
		n = s.nextInt();
		int a[] = new int[n];
		System.out.println("Enter the elements:");
		for (int i = 0; i < n; i++) 
		{
			a[i] = s.nextInt();
		}

		//sorting elements
		for (int i = 0; i < n; i++) 
		{
			for (int j = i + 1; j < n; j++) 
			{
				if (a[i] > a[j]) 
				{
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}

		System.out.println("First largest value is: "+a[n-1]);	
		System.out.println("Third largest value is: "+a[n-3]);
		/* eg:To find 3rd max among 23,678,44,22,11,12  in this case n=6
		      11,12,22,23,44,678
		 n=   0   1  2  3  4  5   
		 so a[n-3]=a[3]=23 */

	}   
}