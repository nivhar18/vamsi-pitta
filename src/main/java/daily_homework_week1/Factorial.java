package daily_homework_week1;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input a number: ");
		int n = sc.nextInt();
		int fact=n;
		for(int i=1;i<=n-1;i++)
		{
			fact*=i;
		}
		System.out.println("The factorial of "+n+" is "+fact);
		sc.close();
	}
}




