package daily_homework_week1;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Input a number: ");
		int n = sc.nextInt();
		int sum = 0, r;
		int temp = n;    
		while(n>0)
		{    
			r = n % 10;   
			sum = (sum*10)+r;    
			n = n/10;    
		}    
		if(temp==sum)    
			System.out.println(temp+" is a Palindrome number");    
		else    
			System.out.println(temp+" is not a palindrome.Since reversal is "+sum);    

		sc.close();
	}
}




