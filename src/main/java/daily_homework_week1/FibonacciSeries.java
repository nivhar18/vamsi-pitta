package daily_homework_week1;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the value of n: ");
		int n = sc.nextInt();
		int fib=0,a=0,b=1;
		System.out.println("The fibonacci series for till "+n+ " is:"+"\n"+a+"\n"+b);
		for(int i=2;i<=n;i++) //since 0 and 1 are already printed using a and b
		{
			
			fib=a+b;   
			System.out.println(fib);
			a=b;
			b=fib;
					
		}
		sc.close();
	}
}




