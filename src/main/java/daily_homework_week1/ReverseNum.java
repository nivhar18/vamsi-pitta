package daily_homework_week1;

import java.util.Scanner;

public class ReverseNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int n = sc.nextInt();
		int sum = 0, r;
		    
		while(n>0)
		{    
			r = n % 10;   
			sum = (sum*10)+r;    
			n = n/10;    
		}    

			System.out.println("The reversed number is "+sum);    

		sc.close();
	}
}




