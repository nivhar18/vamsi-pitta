package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");//create report folder manually
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		//test case level
		ExtentTest test=extent.createTest("TC01_Create_lead", "Create a new lead in leaftaps");
		test.assignCategory("Smoke testing");
		test.assignAuthor("Nivedha");
		//test step level
		test.pass("Browser launched successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data DemoSalesManager entered successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.fail("The data crmsfa not entered");
		test.pass("Login successful");
		extent.flush();

	}

}
