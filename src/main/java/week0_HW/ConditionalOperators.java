package week0_HW;

import java.util.Scanner;

class ConditionalOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Checking relational or conditional operations: ");
		System.out.println("Enter the value of n1 as integer and n2 as float: ");			
		Scanner n1=new Scanner(System.in);
		Scanner n2=new Scanner(System.in);
		int a=n1.nextInt();
		Float b=n2.nextFloat();
		System.out.println("Checking equal to : "+(a==b));
		System.out.println("Checking less than : "+(a<b));
		System.out.println("Checking greater than : "+(a>b));
		System.out.println("Checking less than equal to : "+(a<=b));
		System.out.println("Checking greater than equal to : "+(a>=b));
		System.out.println("Checking not equal to : "+(a!=b));


	}

}
