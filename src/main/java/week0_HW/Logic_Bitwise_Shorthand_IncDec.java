package week0_HW;

import java.util.Scanner;

public class Logic_Bitwise_Shorthand_IncDec {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter the value of n1 and n2: ");			
		Scanner n1=new Scanner(System.in);
		Scanner n2=new Scanner(System.in);
		Scanner n3=new Scanner(System.in);
		Scanner n4=new Scanner(System.in);
		int a=n1.nextInt();
		int b=n2.nextInt();
		int c=n3.nextInt();
		int d=n4.nextInt();
		
		System.out.println("Checking logical operations: ");
		System.out.println("Checking logical AND : "+((a<b)&&(b>c)));
		System.out.println("Checking logical OR : "+((a<b)||(b>c)));
		
		System.out.println("**************************");
		System.out.println("Checking bitwise operations: ");
		System.out.println("Checking bitwise AND : "+((a<b)&(b>c)));
		System.out.println("Checking bitwise OR  : "+((a<b)|(b>c)));
		System.out.println("Checking bitwise XOR  : "+((a<b)^(b<c)));
		//XOR truth table->0^0=0 0^1=0 1^0=0 1^1=0				
		System.out.println("Checking bitwise Complement  : "+~(a));
	    //value of a is +1 in 8-bits=0000 0001 complement is done by
		//flipping values 1111 1110 
		// flip again 0000 0001
		//add 1 to get 0000 0010 which is -2
		
		System.out.println("**************************");
		System.out.println("Checking short hand operations: ");
		System.out.println("Checking shorthand += : "+(a+=2));
		System.out.println("Checking shorthand -=  : "+(a-=b));
		System.out.println("Checking shorthand *=  : "+(c*=4));
		System.out.println("Checking shorthand !=  : "+(a/=b));
		
		System.out.println("**************************");
		System.out.println("Checking Inc/dec operations: ");
		System.out.println("Checking pre-increment : "+(++a));
		System.out.println("Checking post-increment : "+(a++));
		System.out.println("Checking pre-decrement  : "+(--b));
		System.out.println("Checking post-decrement  : "+(b--));
		
		
	}

}
