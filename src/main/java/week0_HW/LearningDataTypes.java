package week0_HW;

public class LearningDataTypes {

	void print()
	{
		boolean a=true;
		byte b=4;
		char c='N';
		short d=20;
		int e=40;
		long f=4474766l;
		float g=69.778899f;
		double h=99.89949454d;
		
		System.out.println("The value and size of primitive data types are:"+"\n"+
				"boolean = "+a+ "\n"+
				"byte = "+b+"\n"+
				"char = "+c+"\n"+
				"short = "+d+"\n"+
				"int = "+e+"\n"+
				"long = "+f+"\n"+
				"float = "+g+"\n"+
				"double = "+h);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LearningDataTypes ld= new LearningDataTypes();
		ld.print();

	}

}
