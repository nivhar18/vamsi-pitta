package week0_HW;

import java.util.Scanner;

public class EvenOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("||| Enter a number to find whether its an even or odd |||");

		Scanner i=new Scanner(System.in);
		int x=i.nextInt();
		if(x%2==0)
			System.out.println(x+" is an even number");
		else
			System.out.println(x+" is an odd number");

	}

}
