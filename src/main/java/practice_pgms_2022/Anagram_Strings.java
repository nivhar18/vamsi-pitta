package practice_pgms_2022;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Anagram_Strings{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter string1: ");
		String s1=sc.nextLine();
		System.out.println("Enter string2: ");
		String s2=sc.nextLine();
		//keep - peek && mother in law - hitler woman
		char [] ca1=s1.toCharArray();
		char [] ca2=s2.toCharArray();

		List<Character> ls1=new ArrayList<Character>();
		for (Character c1 : ca1) {
			ls1.add(c1);
		}
		List<Character> ls2=new ArrayList<Character>();
		for (Character c2 : ca2) {
			ls2.add(c2);
		}
		
		//checking anagram
		if(ls1.containsAll(ls2))
		{
			System.out.println("Strings are anagrams!!!");
		}
		else
		{
			System.out.println("Strings are not anagrams!!!");
		}
		sc.close();
	}}

