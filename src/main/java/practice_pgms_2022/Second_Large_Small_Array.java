package practice_pgms_2022;

import java.util.Scanner;

public class Second_Large_Small_Array {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=s.nextInt();
		int a[]=new int[n];
		int temp;
		System.out.println("Enter the elements:");
		for(int i=0;i<n;i++)
		{
			a[i]=s.nextInt();
		}
		for(int i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}				
			}
		}
		System.out.println("The sorted array:");
		for(int i=0;i<n;i++)
		{
			System.out.println(a[i]);
		}
	System.out.println("Second largest element in array is: "+a[n-2]);
	System.out.println("Second smallest element in array is: "+a[1]);
	}
}
