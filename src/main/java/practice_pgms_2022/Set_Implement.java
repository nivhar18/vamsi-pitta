package practice_pgms_2022;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Set_Implement{
	Set<String> set=new TreeSet<String>();
	
	public void set_add_Contains()
	{
		set.add("samsung");
		set.add("Samsung");
		set.add("Moto");
		set.add("iphone");
		set.add("Oneplus");	
		
		if(set.contains("Oneplus"))
		{
			System.out.println("Set has Oneplus mobile as last element");
		}
	}
	
	public void set_Operations()
	{
		List<String> lst=new ArrayList<String>();
		lst.addAll(set);
		int s=lst.size();
		System.out.println("The set of elements: "+lst);
		System.out.println("The one before last element: "+lst.get(s-2)+
				"\n"+"removing "+lst.remove(s-2)+"\n"+"After removal"+lst);		
	}
	
	public Set_Implement()
	{
		System.out.println("Calling constructor 2 Set_Implement");
	}
	public void overload_fn1(int a,int b)
	{
		System.out.println("Add num "+a+b);
	}
	public void overload_fn1(int a)
	{
		System.out.println("only num "+a);
	}
}
