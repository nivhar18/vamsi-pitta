package practice_pgms_2022;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

//practice websites
//https://omayo.blogspot.com/2013/05/page-one.html
//http://total-qa.com/selenium-webdriver/demo-sites/

public class Website_Practice {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		d.get("http://automationpractice.com/index.php");
		WebElement input=d.findElementByXPath("//input[@id='search_query_top']");
		input.sendKeys("dress");
		Thread.sleep(6000);
		d.findElementByName("submit_search").click();
		WebElement src=d.findElementById("selectProductSort");
		Select dd=new Select(src);
		dd.selectByValue("name:asc");
		Thread.sleep(6000);
		d.findElementByXPath("//a[@title='List']").click();
		
		JavascriptExecutor js = (JavascriptExecutor) d;
		js.executeScript("window.scrollBy(0,1000)");	
		
		d.findElementByXPath("(//a[@title='Add to cart'])[2]").click();
		Thread.sleep(5000);
		d.findElementByXPath("//a[@title='Proceed to checkout']").click();
		String pdetail=d.findElementByXPath("//div[@id='center_column']").getText();
		System.out.println(pdetail);
		js.executeScript("window.scrollBy(0,1000)");	
		d.findElementByXPath("//span[text()='Proceed to checkout']").click();
		d.close();
	}
}


