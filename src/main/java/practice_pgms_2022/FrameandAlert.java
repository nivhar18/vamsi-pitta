package practice_pgms_2022;


import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameandAlert {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.switchTo().frame("iframeResult");
		d.findElementByXPath("//button[text()='Try it']").click();
		Alert pa=d.switchTo().alert();
		System.out.println(pa.getText());
		pa.sendKeys("Nivedha is typing");
		Thread.sleep(2000);
		pa.accept();
		//pa.dismiss();
		WebElement txt=d.findElementByXPath("//p[@id='demo']");
		System.out.println("Result text: "+txt.getText());
		d.switchTo().defaultContent();
		Thread.sleep(2000);
		d.close();
		
		
	}

}
