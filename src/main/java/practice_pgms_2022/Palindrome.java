package practice_pgms_2022;

import java.util.Scanner;

public class Palindrome {
	public void revInt()
	{
		int r,sum=0,temp=0;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the num:");
		int n=s.nextInt();
		temp=n;
		while(n>0)
		{
			r=n%10;
			sum=(sum*10)+r;
			n=n/10;
		}
		if(temp==sum)
			System.out.println("Entered num is palindrome: "+sum);
		else
			System.out.println(temp+" is not a palindrome");		
	}
	public static void main(String[] args) {
		Palindrome p=new Palindrome();
		p.revInt();
	}
}
