package practice_pgms_2022;

import java.util.List;

import javax.management.RuntimeErrorException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeafGroundcheckbox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("https://leafground.com/checkbox.xhtml");
		try {
			List<WebElement> chkbox=d.findElementsByXPath("//span[@class='ui-chkbox-icon ui-icon ui-icon-blank ui-c']");
			int s1=chkbox.size();
			System.out.println("Total num of chkboxes in page: "+s1);
			d.findElementByXPath("(//span[@class='ui-chkbox-icon ui-icon ui-icon-blank ui-c'])[7]/parent::div").click();	
			WebElement toggle=d.findElementByXPath("//div[@class='ui-toggleswitch-slider']/parent::div");
			toggle.click();
		}
		catch (Error e) {
			// TODO: handle exception
			System.out.println("Element not visible exception");
			throw new RuntimeErrorException(e);
		}
		
		finally
		{
			d.close();
		}
	}
}


