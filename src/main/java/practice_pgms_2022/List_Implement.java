package practice_pgms_2022;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class List_Implement implements Collection_Design{
	String s="Amazon India";
	char [] ch=s.toCharArray();
	List<Character> lst=new ArrayList<Character>();
	public void listSort()
	{
		for (char c : ch) {
			lst.add(c);
		}
		System.out.println("The list of elements before sort: "+lst);
		int size=lst.size();
		System.out.println("The last element is: "+ lst.get(size-1));
		Collections.sort(lst);
		System.out.println("The list of elements after sort: ");
		for (char list : lst) {
			System.out.println(list);
		}
		
	}

	public void listReverse()
	{
		Collections.reverse(lst);
		System.out.println("The list of elements after reverse: ");
		for (char revlist : lst) {
			System.out.println(revlist);
		}
	}
	public String listDup(String dup)
	{
		Collections.sort(lst);
		List<Character> removeduplst=lst.stream().distinct().collect(Collectors.toList());
		System.out.println("Removing Duplicate elements: "+removeduplst);
		System.out.println("The 1st element in list: "+lst.get(0));
		return dup;

	}
	public List_Implement()
	{
		System.out.println("Calling constructor 1 List_Implement");
	}
}
