package practice_pgms_2022;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Dynamic_Webtable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		d.get("https://money.rediff.com/gainers/bsc/daily/groupa");
		List<WebElement> cols=d.findElementsByXPath("//*[@id='leftcontainer']/table/thead/tr/th");
		System.out.println("no of cols: "+cols.size());
		List<WebElement> rows=d.findElementsByXPath("//*[@id='leftcontainer']/table/tbody/tr/td[1]");
		System.out.println("no of rows: "+rows.size());
		
		//To get 2nd row's 2nd column data
		WebElement cellNo = d.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[2]/td[2]"));
		String valueNo = cellNo.getText();
		System.out.println("Cell value is : "+valueNo);
		d.close();
	}

}
