package practice_pgms_2022;

import java.util.Scanner;

public class Switch_Prime_StringRev {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the option: "+ "\n" +"1.Prime num 2.String palindrome");
		int opt=s.nextInt();
		switch(opt)
		{
		case 1:
		{
			Scanner s1=new Scanner(System.in);
			System.out.println("Enter the prime num range to find:");
			int num=s1.nextInt();
			for (int i=2;i<=num;i++)
			{
				int count=0;
				for(int j=2;j<=i/2;j++)
				{
					if(i%j==0)
					{
						count++;
						break;
					}}
				if(count==0)
				{
					System.out.println(i);
				}

			}
			s1.close();
			break;
		}
		case 2:
		{
			Scanner s2=new Scanner(System.in);
			System.out.println("Enter the string");
			String str = s2.nextLine();
			String rev="";
			for(int i=str.length()-1;i>=0;i--)
			{
				rev=rev+str.charAt(i);
			}
			if(str.equals(rev))
			{
				System.out.println(str+" is Palindrome");
			}
			else
			{
				System.out.println(str+" is not an Palindrome");
			}
			s2.close();
			break;
		}
		default:
		{
			System.out.println("Invalid option.Please enter option 1 or 2");
			break;
		}		
		}
		s.close();
	}

}
