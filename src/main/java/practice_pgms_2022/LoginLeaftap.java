package practice_pgms_2022;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeaftap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();
		d.manage().window().maximize();
		d.get("http://leaftaps.com/opentaps");
		
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByXPath("//img[@src='/opentaps_images/integratingweb/crm.png']").click();
		d.findElementByXPath("//a[text()='Create Lead']").click();
		
		WebElement src1=d.findElementById("createLeadForm_dataSourceId");
		Select dd1=new Select(src1);
		dd1.selectByVisibleText("Direct Mail");
		
		WebElement src2=d.findElementByName("marketingCampaignId");
		Select dd2=new Select(src2);
		List<WebElement>we=dd2.getOptions();
		int s=we.size();
		dd2.selectByIndex(s-2);
		d.close();
	}

}
