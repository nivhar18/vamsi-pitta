package practice_pgms_2022;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;

public class WindowandSnapshot {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver d=new ChromeDriver(options);//to block popups
		d.manage().window().maximize();
		d.get("https://www.irctc.co.in/nget/train-search");//win 1 
		Thread.sleep(3000);
		d.findElementByXPath("//button[text()='OK']").click();
		Thread.sleep(1000);
		d.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		Thread.sleep(1000);
		d.findElementByXPath("//span[text()='I Agree']").click();
		Thread.sleep(1000);
		d.findElementByXPath("//span[@class='fare-detail-popup-close-sign fa fa-window-close fa-lg']").click();
		d.findElementByXPath("//a[text()=' BUSES ']").click();//win 2
		Set<String> allwin=d.getWindowHandles();
		List<String> list=new ArrayList<>();
		list.addAll(allwin);
		d.switchTo().window(list.get(1));//go to 2nd win
		System.out.println("Title is:"+d.getTitle()+" page URL is: "+d.getCurrentUrl());
		File src=d.getScreenshotAs(OutputType.FILE);
		File dest=new File("./snapshot/snap.png");
		FileUtils.copyFile(src, dest);
		d.switchTo().window(list.get(0));//go to win 1
		d.close();//close win 1
	}

}
