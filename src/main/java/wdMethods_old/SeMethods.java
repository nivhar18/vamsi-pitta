package wdMethods_old;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {
	public RemoteWebDriver driver;
	public int i = 1;

	public void startApp(String browser, String url) {

		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser " + browser + " launched successfully");
		} catch (WebDriverException e) {
			System.err.println("WebDriverException has occured");
			throw new RuntimeException("WebDriverException");
		} catch (NullPointerException e) {
			System.err.println("NullPointerException has occured");
			throw new RuntimeException("NullPointerException");
		} catch (Exception e) {
			System.err.println("Other Exception has occured");
			throw new RuntimeException("Exception occured");
		}

		finally {
			takeSnap();
		}
	}

	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id":
				return driver.findElementById(locValue);
			case "name":
				return driver.findElementByName(locValue);
			case "class":
				return driver.findElementByClassName(locValue);
			case "xpath":
				return driver.findElementByXPath(locValue);
			case "linktext":
				return driver.findElementByLinkText(locValue);
			case "partiallink":
				return driver.findElementByPartialLinkText(locValue);
			case "tag":
				return driver.findElementByTagName(locValue);
			case "css":
				return driver.findElementByCssSelector(locValue);

			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("NoSuchElementException has occured");
		} catch (WebDriverException e) {
			System.err.println("WebDriverException has occured");
			throw new RuntimeException("WebDriverException");
		} catch (Exception e) {
			System.err.println("Other Exception has occured");
			throw new RuntimeException("Exception occured");
		}

		finally {
			takeSnap();
		}
		return null;
	}

	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data " + data + " is entered Successfully");
		takeSnap();
	}

	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element " + ele + " is clicked Successfully");
		takeSnap();
	}

	public void clickWithoutsnap(WebElement ele) {
		ele.click();
		System.out.println("The Element " + ele + " is clicked Successfully");
		// for window close and alerts use this

	}

	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String str = ele.getText();
		System.out.println("Verifying message " + str);

		return str;
	}

	@Override
	public void selectDropDown(WebElement ele, String selectMethod, String value) {

		Select dd = new Select(ele);
		try {
			if (selectMethod.equalsIgnoreCase("visible")) {
				dd.selectByVisibleText(value);
			} else if (selectMethod.equalsIgnoreCase("value")) {
				dd.selectByValue(value);
			} else if (selectMethod.equalsIgnoreCase("index")) {
				dd.selectByIndex(Integer.parseInt(value));
			}
			System.out.println("The dropdown is selected with text " + value);
		} catch (WebDriverException e) {
			System.out.println("The element: " + ele + " could not be found");
		} finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		boolean breturn = false;
		if (driver.getTitle().equals(expectedTitle)) {
			breturn = true;
			System.out.println("The application title:" + driver.getTitle());
		}
		return breturn;

	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String str = ele.getText();
		if (str.equals(expectedText)) {
			System.out.println("The text present is" + str);
		}

	}

	@Override
	public void verifyMatchString(String val, WebElement ele2) {
		// TODO Auto-generated method stub
		String str = ele2.getText();
		if ((driver.getPageSource().contains(val)) && (str.contains(val)) == true) {
			System.out.println("Match found between text!!! and the data is " + str);
		}

	}

	@Override
	public void verifyMatchEle(String str2) {
		// TODO Auto-generated method stub
		if (driver.getPageSource().contains(str2) == true) {
			System.out.println("Match found between elements!!! and the data is " + str2);
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String str = ele.getText();
		if (str.contains(expectedText)) {
			System.out.println("The text present is " + str);
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String str = ele.getAttribute(attribute);
		if (str.equals(value)) {
			System.out.println("The attribute name is " + str);
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String str = ele.getAttribute(attribute);
		if (str.contains(value)) {
			System.out.println("The attribute name is " + str);
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isSelected() == true) {
			System.out.println("The data is selected " + ele.getText());
		} else {
			System.out.println("The data is not selected");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isDisplayed() == true) {
			System.out.println("The data is displayed " + ele.getText());
		} else {
			System.out.println("The data is not displayed");
		}
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> allwin = driver.getWindowHandles();// has two references 0 and 1
		List<String> listwin = new ArrayList<>();
		listwin.addAll(allwin);
		driver.switchTo().window(listwin.get(index));// go to window 2
		System.out.println("Title: "+driver.getTitle()+"\n"+"URL: "+driver.getCurrentUrl());
		driver.manage().window().maximize();

	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert();

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		Alert pa = driver.switchTo().alert();
		pa.accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		Alert pa = driver.switchTo().alert();
		pa.dismiss();
	}

	@Override
	public void getAlertText() {
		Alert pa = driver.switchTo().alert();
		String txt = pa.getText();
		System.out.println("Message in prompt box= " + txt);

	}

	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snapshot/img" + i + ".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void clearElement(WebElement ele) {
		try {
			ele.clear();
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("NoSuchElementException has occured");
		} catch (WebDriverException e) {
			System.err.println("WebDriverException has occured");
			throw new RuntimeException("WebDriverException");
		} catch (Exception e) {
			System.err.println("Other Exception has occured");
			throw new RuntimeException("Exception occured");
		}

		finally {
			takeSnap();
		}
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}

}
