package wdMethods_old;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class TC05_MergeLead extends LoginPage{
	@Test
	public void execute() throws InterruptedException
	{
		login();
		WebElement lead =locateElement("linktext","Leads");
		click(lead);
		WebElement mergelead =locateElement("linktext","Merge Leads");
		click(mergelead);		
		WebElement tolead =locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(tolead);
		switchToWindow(1);
		WebElement fleadid =locateElement("xpath","//input[@id='ext-gen25']");
		type(fleadid,"102");
		WebElement fleadbutton1 =locateElement("xpath","//button[text()='Find Leads']");
		click(fleadbutton1);
		Thread.sleep(1000);
		WebElement firstleadid =locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		clickWithoutsnap(firstleadid);
		switchToWindow(0);		
		WebElement merge =locateElement("linktext","Merge");
		clickWithoutsnap(merge);
		getAlertText();
		acceptAlert();
		WebElement error1 =locateElement("xpath","//li[@class='errorMessage']");
		getText(error1);			
		WebElement fleadlink =locateElement("xpath","//a[text()='Find Leads']");
		click(fleadlink);
		WebElement fromlead =locateElement("xpath","(//div/input)[33]");
		type(fromlead,"10256");
		WebElement fleadbutton2 =locateElement("xpath","//button[text()='Find Leads']");
		click(fleadbutton2);
		WebElement mergelead1 =locateElement("linktext","Merge Leads");
		click(mergelead1);	
		Thread.sleep(1000);
		WebElement from =locateElement("xpath","(//tbody/tr/td/input)[1]");
	    type(from,"10254");
		WebElement to =locateElement("xpath","(//tbody/tr/td/input)[3]");
	    type(to,"10256");
		WebElement merge1 =locateElement("linktext","Merge");
		clickWithoutsnap(merge1);
		getAlertText();
		acceptAlert();		
		closeBrowser();
	}



}
