package wdMethods_old;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class TC04_DuplicateLead extends LoginPage{
	@Test
	public void execute() throws InterruptedException
	{
		login();
		WebElement lead =locateElement("linktext","Leads");
		click(lead);			
		WebElement fleadlink =locateElement("xpath","//a[text()='Find Leads']");
		click(fleadlink);
		WebElement etab =locateElement("xpath","//span[text()='Email']");
		click(etab);
		WebElement email=locateElement("name", "emailAddress");
		type(email, "snivedha277@gmail.com");
		WebElement fleadbutton =locateElement("xpath","//button[text()='Find Leads']");
		click(fleadbutton);
		Thread.sleep(1000);
		WebElement firstleadname =locateElement("xpath","(//tbody/tr/td/div/a)[3]");
		getText(firstleadname);	
		click(firstleadname);
		Thread.sleep(1000);		
		WebElement dup =locateElement("xpath","//a[text()='Duplicate Lead']");
		click(dup);
		WebElement title=locateElement("id","sectionHeaderTitle_leads");
		verifyExactText(title, "Duplicate Lead");
		WebElement create =locateElement("xpath","//input[@value='Create Lead']");
		click(create);		
		WebElement fn=locateElement("xpath","//span[@id='viewLead_firstName_sp']");
		//String b=getText(fn);
		//verifyMatchEle(b);
		closeBrowser();
	}
}
