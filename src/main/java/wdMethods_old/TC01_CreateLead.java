package wdMethods_old;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class TC01_CreateLead extends LoginPage{
	@Test
	public void execute() throws InterruptedException
	{
		login();
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);	
		WebElement compname =locateElement("id","createLeadForm_companyName");
		type(compname, "TCS");
		WebElement firstname =locateElement("id","createLeadForm_firstName");
		type(firstname, "Nivedha");
		WebElement lastname =locateElement("id","createLeadForm_lastName");
		type(lastname, "Harish");
		WebElement source =locateElement("id","createLeadForm_dataSourceId");
		selectDropDown(source,"visible", "Direct Mail");
		WebElement market =locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDown(market,"index","7");
		WebElement fnlocal =locateElement("id","createLeadForm_firstNameLocal");
		type(fnlocal, "Nivi");
		WebElement lnlocal =locateElement("id","createLeadForm_lastNameLocal");
		type(lnlocal, "Sam");
		WebElement salute =locateElement("name","personalTitle");
		type(salute,"Mrs.");
		WebElement title =locateElement("name","generalProfTitle");
		type(title,"Testleaf");
		WebElement dept =locateElement("xpath","(//input[@class='inputBox'])[10]");
		type(dept,"IT");
		WebElement revenue =locateElement("xpath","(//input[@class='inputBox'])[11]");
		type(revenue,"10000");
		WebElement curr =locateElement("id","createLeadForm_currencyUomId");
		selectDropDown(curr,"value","INR");
		WebElement inds =locateElement("id","createLeadForm_industryEnumId");
		selectDropDown(inds,"value","IND_SOFTWARE");
		WebElement own =locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDown(own,"value","OWN_CCORP");
		WebElement noe =locateElement("xpath","(//input[@class='inputBox'])[12]");
		type(noe,"10 million");
		WebElement sic =locateElement("xpath","(//input[@class='inputBox'])[13]");
		type(sic,"23244");
		WebElement ticker =locateElement("xpath","(//input[@class='inputBox'])[14]");
		type(ticker,"yes");
		WebElement desc =locateElement("xpath","(//textarea[@class='inputBox'])[1]");
		type(desc,"10M dollar company");
		WebElement imp =locateElement("xpath","(//textarea[@class='inputBox'])[2]");
		type(imp,"best employer");		
		WebElement ccode =locateElement("id","createLeadForm_primaryPhoneCountryCode");
		clearElement(ccode);
		type(ccode,"91");		
		WebElement area =locateElement("xpath","(//input[@class='inputBox'])[16]");
		type(area,"044");
		WebElement phn =locateElement("xpath","(//input[@class='inputBox'])[17]");
		type(phn,"9548565653");
		WebElement ext =locateElement("xpath","(//input[@class='inputBox'])[18]");
		type(ext,"664");
		WebElement person =locateElement("xpath","(//input[@class='inputBox'])[19]");
		type(person,"Siva");
		WebElement email =locateElement("xpath","(//input[@class='inputBox'])[20]");
		type(email,"snivedha277@gmail.com");
		WebElement web =locateElement("xpath","(//input[@class='inputBox'])[21]");
		type(web,"na");
		WebElement toname =locateElement("xpath","(//input[@class='inputBox'])[22]");
		type(toname,"john");
		WebElement attn =locateElement("xpath","(//input[@class='inputBox'])[23]");
		type(attn,"lion");
		WebElement al1 =locateElement("xpath","(//input[@class='inputBox'])[24]");
		type(al1,"14");
		WebElement al2 =locateElement("xpath","(//input[@class='inputBox'])[25]");
		type(al2,"Lakshmi street");
		WebElement city =locateElement("xpath","(//input[@class='inputBox'])[26]");
		type(city,"chennai");
		WebElement country =locateElement("id","createLeadForm_generalCountryGeoId");
		selectDropDown(country,"visible","India");
		Thread.sleep(1000);
		WebElement state =locateElement("id","createLeadForm_generalStateProvinceGeoId");
		selectDropDown(state,"visible","TAMILNADU");
		Thread.sleep(1000);
		WebElement postal =locateElement("xpath","(//input[@class='inputBox'])[27]");
		type(postal,"600083");
		WebElement zipext =locateElement("xpath","(//input[@class='inputBox'])[28]");
		type(zipext,"603");
		WebElement create =locateElement("xpath","(//input[@value='Create Lead'])[1]");
		click(create);
		WebElement fn =locateElement("xpath","(//span[text()='Nivedha'])[1]");
		verifyMatchString("Nivedha", fn);	
		WebElement eleLogout = locateElement("linktext", "Logout");
		click(eleLogout);	
	}



}
