package warmup_2020;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Framealert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		d.switchTo().frame("iframeResult");
		d.findElementByXPath("//button[text()='Try it']").click();		
		Alert al=d.switchTo().alert();
		System.out.println("the msg in prompt box: "+al.getText());
		al.sendKeys("nivhar");
		al.accept();
		WebElement p=d.findElementByXPath("(//p[@id='demo'])");
		System.out.println("result: "+p.getText());
		d.switchTo().defaultContent();
		d.close();
	}

}
