package warmup_2020;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Listfn {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Select the option:1)sort 2)reverse sort with lowercase 3)removing duplicates ");
		int opt=s.nextInt();
		switch (opt) {
		case 1:
			System.out.println("enter the string to sort");
			String str=s.next();
			char[] chr=str.toCharArray();
			List<Character> list=new ArrayList<Character>();
			for (char c : chr) {
				list.add(c);
			}
			Collections.sort(list);
			System.out.println(list);
			break;
		case 2:
			System.out.println("enter the string to reverse and lowercase");
			String str1=s.next();
			str1=str1.toLowerCase();
			char[] chr1=str1.toCharArray();
			List<Character> list1=new ArrayList<Character>();
			for (char c : chr1) {
				list1.add(c);
			}
			Collections.reverse(list1);
			System.out.println(list1);
			break;
		case 3:
			System.out.println("enter the string to remove duplicates");
			String str2=s.next();
			str2=str2.toLowerCase();
			char[] chr2=str2.toCharArray();
			List<Character> list2=new ArrayList<Character>();
			for (char c : chr2) {
				if(!list2.contains(c))
				list2.add(c);
			}
			Collections.sort(list2);
			System.out.println(list2);
			break;

		default:
			System.out.println("please enter a valid option 1 to 3");
			break;
		}		
				
		s.close();
	}

}
