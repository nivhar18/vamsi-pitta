package warmup_2020;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Loginxpath {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");//set binding
		ChromeDriver d=new ChromeDriver();//open browser
		d.manage().window().maximize();//maximize
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);//makes all elements in page wait for 5 sec for loading
		d.get("http://leaftaps.com/opentaps");
		try {
			d.findElementById("username").sendKeys("DemoSalesManager");
			d.findElementById("password").sendKeys("crmsfa");
			d.findElementByClassName("decorativeSubmit").click();
			d.findElementByLinkText("CRM/SFA").click();
			d.findElementByLinkText("Create Lead").click();
			d.findElementById("createLeadForm_companyName").sendKeys("tcs");
			d.findElementById("createLeadForm_firstName").sendKeys("niv");
			d.findElementById("createLeadForm_lastName").sendKeys("har");
			
			WebElement src=d.findElementById("createLeadForm_dataSourceId");
			Select dd=new Select(src);
			dd.selectByVisibleText("Conference");
			
			WebElement src1=d.findElementById("createLeadForm_marketingCampaignId");
			Select dd1=new Select(src1);
			List<WebElement> lst=dd1.getOptions();
			int size=lst.size();
			dd1.selectByIndex(size-2);
			
			WebElement src2=d.findElementById("createLeadForm_industryEnumId");
			Select dd2=new Select(src2);
			dd2.selectByValue("IND_AEROSPACE");
			Thread.sleep(3000);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("NoSuchElementExceptionException occured"); throw new RuntimeException();
		}	
		finally {
			d.close();
		}
	}

}
