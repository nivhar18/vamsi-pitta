package warmup_2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://leaftaps.com/opentaps");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click();
		d.findElementByLinkText("Leads").click();
		
		d.findElementByLinkText("Merge Leads").click();
		d.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> s= d.getWindowHandles();
		List<String> lst = new ArrayList<String>();
		lst.addAll(s);
		d.switchTo().window(lst.get(1));
		d.findElementByXPath("//*[@id='ext-gen25']").sendKeys("10100");
		d.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		d.findElementByXPath("(//a[contains(text(),'10')]/parent::div)[1]").click();
		Thread.sleep(1000);
		d.switchTo().window(lst.get(0));
		d.findElementByLinkText("Merge").click();
		Alert a=d.switchTo().alert();
		a.accept();
		/*d.findElementByLinkText("Find Leads").click();
		d.findElementByXPath("//*[@id='ext-gen246']");
		d.findElementByXPath("//button[text()='Find Leads']").click();*/
		WebElement txt=d.findElementByXPath("(//div[@class='messages'])");
		System.out.println(txt.getText());
	
			d.close();
		
		
		
		
		
		
		
		

	}

}
