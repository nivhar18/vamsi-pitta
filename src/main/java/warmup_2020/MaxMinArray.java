package warmup_2020;

import java.util.Scanner;

public class MaxMinArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter the size of array");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int a[]=new int[n];
		for(int i=0;i<n;i++)
		{
			a[i]=sc.nextInt();
		}
		int temp=a[0];
		for(int i=1;i<n;i++)
		{
			if(temp<a[i])
			{
				temp=a[i];
			}
		}
		int temp1=a[0];
		System.out.println("The max value is "+temp);
		for(int i=1;i<n;i++)
		{
			if(temp1>a[i])
			{
				temp1=a[i];
			}
		}
		System.out.println("The min value is "+temp1);
		
	}

}
