package warmup_2020;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OnlyNumInStr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the string ");
		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine();	//Hello 1 World 2 test 3
		String m=s.replaceAll("\\D","");//since strings are immutable(values are static) assign here;
		System.out.println(m);
		sc.close();

	}

}
