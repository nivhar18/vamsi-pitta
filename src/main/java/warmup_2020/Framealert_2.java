package warmup_2020;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Framealert_2 {
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		d.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		d.switchTo().frame("iframeResult");
		d.findElementByXPath("//button[text()='Try it']").click();
		Alert a=d.switchTo().alert();
		a.sendKeys("nivedha");
		Thread.sleep(1000);
		a.accept();
		WebElement txt=d.findElementByXPath("//p[@id='demo']");
		System.out.println(txt.getText());
		d.switchTo().defaultContent();
		d.close();
		
	}


}