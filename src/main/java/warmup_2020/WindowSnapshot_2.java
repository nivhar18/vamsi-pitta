package warmup_2020;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WindowSnapshot_2 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver d=new ChromeDriver(options);//to block popups
		d.manage().window().maximize();
		d.get("https://www.irctc.co.in/nget/train-search");//0
		d.findElementByXPath("(//span[text()='AGENT LOGIN']/parent::a)").click();//0
		d.findElementByXPath("(//a[text()='Contact Us'])").click();//0 and opens 1
		 Set<String> s= d.getWindowHandles();//0 n 1
		 List<String> lst=new ArrayList<>();
		 lst.addAll(s);
		 d.switchTo().window(lst.get(1));
		System.out.println("title: "+d.getTitle()+"curr url: "+d.getCurrentUrl());
		d.manage().window().maximize();
		File src=d.getScreenshotAs(OutputType.FILE);
		File dest=new File("./sample/img.png");
		FileUtils.copyFile(src, dest);
		d.switchTo().window(lst.get(1));
		d.close();
		
		 
		 
		
		
	}

}
