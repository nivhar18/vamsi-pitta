package warmup_2020;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Mapfn {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		char[] ch=str.toCharArray();
		Map<Character, Integer> mp=new HashMap<Character, Integer>();
		for (char c : ch) {
			if(mp.containsKey(c))
			{
				int val=mp.get(c)+1;
				mp.put(c, val);
			}
			else
			{
				mp.put(c, 1);
			}
			
		}
		System.out.println(mp);
		sc.close();
		
	}

}
