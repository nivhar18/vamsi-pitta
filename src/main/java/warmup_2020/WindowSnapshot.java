package warmup_2020;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowSnapshot {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();//win 0
		d.get("https://www.irctc.co.in/nget/train-search");//window 1
		d.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		d.findElementByXPath("(//a[text()='Contact Us'])").click();
		Set<String> allwin =d.getWindowHandles();//win 0 and 1
		List<String> lst=new ArrayList<>();
		lst.addAll(allwin);
		d.switchTo().window(lst.get(1));//switch to win 1
		System.out.println("title: "+d.getTitle()+"curr url: "+d.getCurrentUrl());
		d.manage().window().maximize();//win 1
		File src=d.getScreenshotAs(OutputType.FILE);
		File dest=new File("./screenshot/contact.png");
		FileUtils.copyFile(src, dest);
		d.switchTo().window(lst.get(0));//win 0 get and close		
		d.close();
		d.switchTo().window(lst.get(1));//win 1 get and close
		d.close();
		
		
	}

}
