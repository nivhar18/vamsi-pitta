package warmup_2020;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Checkboxpath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		d.get("http://testleaf.herokuapp.com/pages/table.html");
		d.findElementByXPath("(//input[@type='checkbox'])[3]").click();//last one
		List<WebElement> lst=d.findElementsByXPath("//input[@type='checkbox']");
		int s=lst.size();
		System.out.println("no of check boxes: "+s);//total nos of chk boxes
		lst.get(s-2).click();//last but one
		
	}

}
