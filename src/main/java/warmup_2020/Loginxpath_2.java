package warmup_2020;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class Loginxpath_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");//to set file binding
		ChromeDriver d=new ChromeDriver();//to open chrome browser 
		d.manage().window().maximize();//to maximize browser
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			d.get("http://leaftaps.com/opentaps/control/login");
			d.findElementById("username").sendKeys("DemoSalesManager");
			d.findElementById("password").sendKeys("crmsfa");
			d.findElementByXPath("//input[@type='submit']").click();
			d.findElementByXPath("//img[@src='/opentaps_images/integratingweb/crm.png']").click();
			d.findElementByLinkText("Create Lead").click();
			Thread.sleep(2000);
			d.findElementById("createLeadForm_companyName").sendKeys("tcs");
			d.findElementById("createLeadForm_firstName").sendKeys("niv");
			d.findElementById("createLeadForm_lastName").sendKeys("har");
			
			WebElement s1=d.findElementById("createLeadForm_dataSourceId");
			Select d1=new Select(s1);
			d1.selectByVisibleText("Cold Call");//visible text in option
			
			WebElement s2=d.findElementById("createLeadForm_industryEnumId");
			Select d2=new Select(s2);
			d2.selectByValue("IND_SOFTWARE");//value attr in option
			
			WebElement s3=d.findElementById("createLeadForm_currencyUomId");
			Select d3=new Select(s3);
			List<WebElement> lst=d3.getOptions();
			int size=lst.size();
			d3.selectByIndex(size-1);//last ele
			
			WebElement cc=d.findElementByName("primaryPhoneCountryCode");
			cc.clear();//clear the default value
			Thread.sleep(2000);//1 sec
			cc.sendKeys("91");
			
			Thread.sleep(5000);
			String title=d.getTitle();
			System.out.println("heading of page"+title);
						
		} 	
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("exception occured -"+e.getMessage());
			
		}
		finally
		{
			d.close();
		}

	}

}
