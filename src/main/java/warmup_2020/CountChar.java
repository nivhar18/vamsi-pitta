package warmup_2020;

import java.util.HashMap;
import java.util.Scanner;

public class CountChar {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a string");
		String str=sc.nextLine();
		char chr[]=str.toCharArray();
		HashMap<Character, Integer> map = new HashMap<>();
		for (char ch : chr) {
			if (map.containsKey(ch)) { 
				int val = map.get(ch);//get the value of char
				map.put(ch, val+1);//inc by 1 as get value is 0 by default
				//value of a already exists so a=0+1+1
			} else {
				map.put(ch, 1);//only one occurence then 1
			}
		}
		System.out.println(map);
		sc.close();
	}

}




