package warmup_2020;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Checkboxpath_2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.get("http://leafground.com/pages/table.html");
		
		//to find no of columns using table headers
		WebElement col=d.findElementByXPath("(//table[@id='table_id']/tbody)");
		List<WebElement> lst=col.findElements(By.tagName("th"));
		System.out.println("total no of cols: "+lst.size());
		
		//to find no of rows excluding header (using copy xpath)
		List<WebElement> lst1=d.findElements(By.xpath("//*[@id='table_id']/tbody/tr/td[1]"));
		System.out.println("total no of rows: "+lst1.size());
		
		//value of learn ele (using copy xpath)
		WebElement val=d.findElementByXPath("//*[@id=\"table_id\"]/tbody/tr[3]/td[2]");
		System.out.println("progress value of learn ele by gopi: "+val.getText());
		
		//least progress check
		List<WebElement> l2=d.findElements(By.xpath("//input[@type='checkbox']"));
		l2.get(l2.size()-1).click();
		System.out.println("least progress checked");	
		Thread.sleep(3000);
		
		//uncheck the selected checkbox
		if(l2.get(l2.size()-1).isSelected()==true)
		{
			l2.get(l2.size()-1).click();
			Thread.sleep(3000);
		}
		
		//count calc
		d.get("http://leafground.com/pages/checkbox.html");
		List<WebElement> chk=d.findElementsByXPath("//input[@type='checkbox']");
		int checkcount=0,uncheckcount=0;
		for (WebElement c : chk) {
			if(c.isSelected()==true)
			{
				checkcount++;
			}
			else
			{
				uncheckcount++;
			}
		}
		System.out.println("chkcount: "+checkcount+"\n"+"unchkcount: "+uncheckcount);
		d.close();
		
		
	}

}
