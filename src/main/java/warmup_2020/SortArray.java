package warmup_2020;

import java.util.Scanner;

public class SortArray {
	public static void main(String[] args) {

		Scanner s=new Scanner(System.in);
		System.out.println("enter the range of array");
		int n=s.nextInt();
		int temp;
		int a[]=new int[n];
		System.out.println("enter the elements of array");
		for(int i=0;i<n;i++)
		{
			a[i]=s.nextInt();
		}
		for(int i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(a[i]>a[j])
				{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
				}
			}
		}
		System.out.println("result in asc order");
		for(int i=0;i<n;i++)
		{
			System.out.println(a[i]);
		}
		
	}

}
