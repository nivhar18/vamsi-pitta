package Java_Interview_pgms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class P8_ReadWrite_Text {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 try {
	            FileWriter writer = new FileWriter("D:\\Sample.txt", true);
	            BufferedWriter bufferedWriter = new BufferedWriter(writer);
	 
	            bufferedWriter.write("Hello World");
	            bufferedWriter.newLine();
	            bufferedWriter.write("See You Again!");
	 
	            bufferedWriter.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		 
		 try {
	            FileReader reader = new FileReader("D:\\Sample.txt");
	            BufferedReader bufferedReader = new BufferedReader(reader);
	 
	            String line;
	 
	            while ((line = bufferedReader.readLine()) != null) {
	                System.out.println(line);
	            }
	            reader.close();
	 
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	}

}
