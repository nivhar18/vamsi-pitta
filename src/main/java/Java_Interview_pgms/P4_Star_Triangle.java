package Java_Interview_pgms;

import java.util.Scanner;

public class P4_Star_Triangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no of rows for inc * triangle:");
		int n=sc.nextInt();
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=i;j++)
			{
				System.out.print("*");
			}
			System.out.println(" ");
		}
		System.out.println("Enter no of rows for dec * triangle:");
		int x=sc.nextInt();
		for(int i=1;i<=x;i++)
		{
			for(int j=x;j>=i;j--)
			{
				System.out.print("*");
			}
			System.out.println(" ");
		}

	}

}
