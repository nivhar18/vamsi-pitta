package Java_Interview_pgms;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class P6_Occur_char {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");//$$ Welcome to 1st Automation Interview $$ 
		String str=sc.nextLine();
		char[] ch=str.toCharArray();
		Map<Character,Integer> m=new HashMap<>();
		for(char c:ch)
		{
			if(m.containsKey(c))
			{
				int val=m.get(c);
				m.put(c, val+1);
			}
			else
			{
				m.put(c, 1);
			}
			
		}
		System.out.println(m);

	}

}
