package Java_Interview_pgms;

import java.util.Scanner;

public class P7_MaxMin_Array {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=s.nextInt();
		int a[]=new int[n];
		System.out.println("Enter elements of array");
		for(int i=0;i<n;i++)
		{
			a[i]=s.nextInt();
		}
		int temp=a[0];
		for(int i=1;i<n;i++)
		{
			if(temp<a[i])
				temp=a[i];
		}
		System.out.println("Max value in array: "+temp);
		
		int temp1=a[0];
		for(int i=1;i<n;i++)
		{
			if(temp1>a[i])
				temp1=a[i];
		}
		System.out.println("Min value in array: "+temp1);
		
		
	}

}
