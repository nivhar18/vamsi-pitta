package daily_homework_week4;

import java.util.Scanner;

public class NumDig {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the Number:");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		System.out.println("Enter the digit 1-9:");
		int digi = scan.nextInt();
		scan.close();
		num--;
		while(Integer.toString(num).contains(Integer.toString(digi))){
			num--;
		}
		System.out.println("Largest number:"+num);
	}

}
