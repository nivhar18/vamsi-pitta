package daily_homework_week4;

import java.text.DecimalFormat;
import java.util.Scanner;

public class PercentOfChar {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String");
		String str=sc.nextLine();
		int size=str.length();
		int uc=0,lc=0,dig=0,other=0;
		for(int i=0;i<size;i++)
		{
			char ch=str.charAt(i);
			if (Character.isUpperCase(ch)) {  
				uc++;  
			}  
			else if (Character.isLowerCase(ch)) {  
				lc++;  
			}  
			else if (Character.isDigit(ch)) {  
				dig++;  
			}  
			else {  
				other++;  
			}  
		}
		
		double upperCaseLetterPercentage = (uc * 100) / size;  
        double lowerCaseLetterPercentage = (lc * 100) / size;  
        double digitsPercentage = (dig * 100) / size;  
        double otherCharPercentage = (other * 100) / size;  
   
        DecimalFormat formatter = new DecimalFormat("##.##");
        System.out.println("In '" + str + "' : ");  
        System.out.println("Number of Uppercase letters is "+uc+" So,percentage is " + formatter.format(upperCaseLetterPercentage) + " % ");  
        System.out.println("Number of Lowercase letters is "+lc+" So,percentage is " + formatter.format(lowerCaseLetterPercentage) + " %");  
        System.out.println("Number of digits is "+dig+" So,percentage is " + formatter.format(digitsPercentage) + " %");  
        System.out.println("Number of other is "+other+" So,percentage is " + formatter.format(otherCharPercentage) + " %");  
   //Tiger Runs @ The Speed Of 100 km/hour.
	}

}
