package wdMethods_new;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ZoomCar extends ZoomMethods{


	@BeforeTest
	public void setData()
	{
		testCaseName="Zoomcar";
		testCaseDesc="Accessing zoomcar";
		category="smoke";
		author="nivedha";
		
	}
	@Test
	public void zoomcar() throws InterruptedException
	{
		
		WebElement start = locateElement("linktext", "Start your wonderful journey");
		click(start);
		Thread.sleep(2000);
		WebElement pickup=locateElement("xpath","(//div[@class='component-popular-locations']/*)[3]");
		click(pickup);
		WebElement nxt=locateElement("xpath","//button[text()='Next']");
		click(nxt);
		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);
		Thread.sleep(2000);
		WebElement nxtday=locateElement("xpath","(//div[text()[contains(.,'"+tomorrow+"')]])");
		click(nxtday);//div[contains(text(),'17')]
		String a=nxtday.getText();
		nxt=locateElement("xpath","//button[text()='Next']");
		click(nxt);
		nxtday=locateElement("xpath","(//div[text()[contains(.,'"+tomorrow+"')]])");
		String b=nxtday.getText();
		if(a.equals(b))
		{
			System.out.println("Confirmed start date is "+a);
		}
		WebElement done=locateElement("xpath","//button[text()='Done']");
		click(done);
		Thread.sleep(2000);
		List<WebElement> Result= driver.findElementsByXPath("//div[@class='component-car-item']");
		int size = Result.size();
		System.out.println("The Number of Cars " +size);
		List<WebElement> price = driver.findElementsByClassName("price");
		List<Integer> allPrices=new ArrayList<Integer>();
		for (WebElement eachPrice : price) {
			allPrices.add(Integer.parseInt(eachPrice.getText().replaceAll("\\D", "")));
			//get text each price and add to array list
			//replace all non digit character with "" no space i,e to ignore non dig char
		
		}
	
	int maxValue = Collections.max(allPrices);
	System.out.println("The max value of car "+maxValue);
	WebElement MaxBrand = locateElement("xpath","(//div[@class='car-book']//div[contains(text(), '"+maxValue+"')]/preceding::div[@class='details'][1]/h3)");
	//div[@class='car-amount']/following::div[contains(text(),'520')]
	String text = getText(MaxBrand);
	System.out.println("The max value car brand name"+text);
	WebElement bookNow = locateElement("xpath","(//div[@class='car-book']//div[contains(text(), '"+maxValue+"')]/following-sibling::button)");
	click(bookNow);


}
}
