package wdMethods_new;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FacebookLikes {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver d=new ChromeDriver(options);//to block popups
		d.manage().window().maximize();
		d.get("https://www.facebook.com");
		d.findElementById("email").sendKeys("9445653650");
		d.findElementById("pass").sendKeys("Mytcs@15");
		d.findElementByXPath("//input[@value='Log In']").click();
		Thread.sleep(4000);
		d.findElementByXPath("(//input[@type='text']/following::input[@data-testid='search_input'])[1]").sendKeys("TestLeaf");
		d.findElementByXPath("(//button[@data-testid='facebar_search_button'])").click();	
		Thread.sleep(4000);
		String c=d.findElementByXPath("(//span[text()='Places']/following::div[text()='TestLeaf'])[1]").getText();
		System.out.println("Verifying text is "+ c);			
		String a=d.findElementByXPath("(//div[text()='TestLeaf']/following::span/button[@type='submit'])[1]").getText();
		System.out.println("The text present is "+ a);
		if(a.equals("Like"))
		{
		d.findElementByXPath("(//div[text()='TestLeaf']/following::span/button[@type='submit'])[1]").click();
		}
		else
		{
		String b=d.findElementByXPath("(//div[text()='TestLeaf']/following::span/button[@type='submit'])[1]").getText();
		System.out.println("The like button is already "+ b);
		}
		d.findElementByXPath("(//span[text()='Places']/following::div[text()='TestLeaf'])[1]").click();
		String e=d.getTitle();
		if(e.contains("TestLeaf"))
		{
			System.out.println("The title contains text: "+e);
		}
		Thread.sleep(4000);
		String f=d.findElementByXPath("//div[contains(text(),'people like this')]").getText();
		System.out.println("The number of likes: "+f);
		d.quit();
	}

}
