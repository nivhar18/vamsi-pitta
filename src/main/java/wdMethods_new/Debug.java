// SeMethods class NOT extended.

package wdMethods_new;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Debug {

	@Test
	public static void main(String[] args) throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();
        Thread.sleep(5000);
		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span")
				.getText()
				.replaceAll("[^0-9]", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
				.getText()
				.replaceAll("\\D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		//driver.findElementByXPath("//h3[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<Integer> onlyPrice = new ArrayList<Integer>();

		for (WebElement productPrice : productsPrice) {
			String rp1 = productPrice.getText().replaceAll("\\D", "");
			int rp2 = Integer.parseInt(rp1);
			onlyPrice.add(rp2);
		}

		// Sort them 
		int max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		//driver.close();

		// Print Only Allen Solly Brand Minimum Price
		WebElement wb1 = driver.findElementByXPath("//div[@class='brand-more']");
		wb1.click();
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']");
		Thread.sleep(1000);

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<Integer> onlyPrice1 = new ArrayList<Integer>();
		for (WebElement pp1 : allenSollyPrices) {
			String rp3 = pp1.getText().replaceAll("\\D", "");
			int rp4 = Integer.parseInt(rp3);	
		onlyPrice1.add(rp4);
		}

		// Get the minimum Price 
		int min = Collections.min(onlyPrice1);

		// Find the lowest priced Allen Solly
		System.out.println(min);

	}

}
