package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UpdateLead extends ProjectMethods{
	public UpdateLead()
	{
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	
	@FindBy(id="updateLeadForm_companyName")
	WebElement compname;
	public UpdateLead typecomp(String data)
	{
		clearElement(compname);
		type(compname,data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//input[@value='Update']")
	WebElement update;
	public ViewEdit clickupdate() throws InterruptedException
	{
		click(update);
		sleepTime();
		return new ViewEdit();
	}
	
	
	

}
