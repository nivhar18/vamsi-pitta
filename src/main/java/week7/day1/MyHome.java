package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;


public class MyHome extends ProjectMethods {
	public MyHome()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead")
	WebElement createlead;
	@When("click on create lead")
	public CreateLead clicklead()
	{
		click(createlead);
		return new CreateLead();
	}



}
