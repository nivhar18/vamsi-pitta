package week7.day1;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;




public class TC02_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC02_EditLead";
		testCaseDesc="Edit an existing lead";
		category="smoke";
		author="nivedha";
		excelfilename="editlead";
		
	}
	@Test(dataProvider="fetchData")
	public void createLeadExecute(String user,String pwd,String fn,String vtit,String comp,String vcomp) throws InterruptedException
	{
		new LoginPage().typeUn(user).typePwd(pwd).clickLogin();
		new HomePage().clickhome();
		new MyHomeEdit().clicklead();
		new leadMenu().clickfl();
		new FindLead().typefn(fn).clickfind().clickflead();
		new ViewEdit().verifytitle(vtit).clickEdit();
		new UpdateLead().typecomp(comp).clickupdate();
		new ViewEdit().verifycname(vcomp).clickLogout();
		
		
		
		
	}



}
