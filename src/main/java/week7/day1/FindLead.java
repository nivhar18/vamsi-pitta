package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;



public class FindLead extends ProjectMethods{
	public FindLead()
	{
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	WebElement fn;
	public FindLead typefn(String data)
	{
		type(fn,data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']")
	WebElement findbut;
	public FindLead clickfind() throws InterruptedException
	{
		click(findbut);
		sleepTime();
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//tbody/tr/td/div/a)[1]")
	WebElement firstlead;
	public ViewEdit clickflead() throws InterruptedException
	{
		click(firstlead);
		sleepTime();
		return new ViewEdit();
	}
	
	
	

}
