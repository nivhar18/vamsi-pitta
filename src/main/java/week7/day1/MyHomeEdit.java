package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class MyHomeEdit extends ProjectMethods {
	public MyHomeEdit()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="Leads")
	WebElement lead;
	public leadMenu clicklead()
	{
		click(lead);
		return new leadMenu();
	}



}
