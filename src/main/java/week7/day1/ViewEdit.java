package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ViewEdit extends ProjectMethods{
	public ViewEdit()
	{
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	
	public ViewEdit verifytitle(String expectedText)
	{
		verifyTitle(expectedText);//View Lead | opentaps CRM
		return this;
	}

	
	@FindBy(how=How.LINK_TEXT,using="Edit")
	WebElement edit;
	public UpdateLead clickEdit() throws InterruptedException
	{
		click(edit);
		sleepTime();
		return new UpdateLead() ;
	}
	
	@FindBy(how=How.ID,using="viewLead_companyName_sp")
	WebElement verifycomp;
	public ViewEdit verifycname(String expectedText)
	{
		verifyExactText(verifycomp, expectedText);
		return this;
	}
	@FindBy(how=How.XPATH,using="(//a[text()='Logout'])")
	WebElement logout;
	public HomePage clickLogout()
	{
		click(logout);
		return new HomePage();
	}
	

}
