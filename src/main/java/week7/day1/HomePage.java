package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
		PageFactory.initElements(driver,this);
	}
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
	WebElement crmsfa;
	@When("click on crmsfa")
	public MyHome clickhome()
	{
		click(crmsfa);
		return new MyHome();
	}
}
