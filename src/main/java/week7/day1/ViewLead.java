package week7.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;

public class ViewLead extends ProjectMethods{
	public ViewLead() {
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	@FindBy(how=How.XPATH,using="(//span[@id='viewLead_firstName_sp'])")
	WebElement verifyfn;
	@Then("Verify first name as (.*)")
	public ViewLead verifyfname(String expectedText)
	{
		verifyExactText(verifyfn, expectedText);
		return this;
	}
	@FindBy(how=How.XPATH,using="(//a[text()='Logout'])")
	WebElement logout;
	public HomePage clickLogout()
	{
		click(logout);
		return new HomePage();
	}
	
}
