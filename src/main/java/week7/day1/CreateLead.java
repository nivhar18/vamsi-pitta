package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;



public class CreateLead extends ProjectMethods{
public CreateLead() {
	PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement cn;
	@When("Enter the company name as (.*)")
	public CreateLead typeCn(String data)
	{
		type(cn,data);
		return this;
	}
	@FindBy(id="createLeadForm_firstName")
	WebElement fn;
	@When("Enter the first name as (.*)")
	public CreateLead typeFn(String data)
	{
		type(fn,data);
		return this;
	}
	@FindBy(id="createLeadForm_lastName")
	WebElement ln;
	@When("Enter the last name as (.*)")
	public CreateLead typeLn(String data)
	{
		type(ln,data);
		return this;
	}
	@FindBy(how=How.XPATH,using="(//input[@value='Create Lead'])[1]")
	WebElement create;
	@When("click on create button")
	public ViewLead clickCreate()
	{
		click(create);
		return new ViewLead();
	}
	}

