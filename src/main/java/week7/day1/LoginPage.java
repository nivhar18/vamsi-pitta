package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;



public class LoginPage extends ProjectMethods{
	public LoginPage()
	{
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	@FindBy(id="username")
	WebElement un;
	@And("Enter the UserName as (.*)")
	public LoginPage typeUn(String data)
	{
		type(un,data);
		return this;
	}
	@FindBy(id="password")
	WebElement pwd;
	@Given("Enter the Password as (.*)")
	public LoginPage typePwd(String data)
	{
		type(pwd,data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
	WebElement login;
	@When("click on the login button")
	public HomePage clickLogin()
	{
		click(login);
		return new HomePage();
	}
	
	
	

}
