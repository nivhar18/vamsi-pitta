package week7.day1;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;




public class TC01_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC01_CreateLead";
		testCaseDesc="Create a new lead";
		category="smoke";
		author="nivedha";
		excelfilename="CreateLead-PageModel";
		
	}
	@Test(dataProvider="fetchData")
	public void createLeadExecute(String user,String pwd,String cname,String fn,String ln,String verify) throws InterruptedException
	{
		new LoginPage().typeUn(user).typePwd(pwd).clickLogin();
		new HomePage().clickhome();
		new MyHome().clicklead();
		new CreateLead().typeCn(cname).typeFn(fn).typeLn(ln).clickCreate();
		new ViewLead().verifyfname(verify).clickLogout();
		
		
	}



}
