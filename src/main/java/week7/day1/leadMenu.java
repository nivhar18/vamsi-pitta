package week7.day1;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;



public class leadMenu extends ProjectMethods{
	public leadMenu()
	{
		PageFactory.initElements(driver,this);//declare driver globally for pointing that page(this)
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Find Leads']")
	WebElement fl;
	public FindLead clickfl()
	{
		click(fl);
		return new FindLead();
	}
	
	
	

}
