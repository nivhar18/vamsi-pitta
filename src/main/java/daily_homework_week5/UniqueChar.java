package daily_homework_week5;

public class UniqueChar {

	public static void main(String[] args) {
		String input="Good looking";
		String output="";
		char b[]=input.toLowerCase().replace(" ", "").toCharArray();
		System.out.println("Input:"+input);
	
		for (int i=0;i<b.length;i++) {
			if(output.indexOf(b[i]) == -1) {
				output = output + b[i];
			}
		}
		System.out.println("Output:"+output);
		
	}

}
