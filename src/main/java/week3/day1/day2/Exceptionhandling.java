package week3.day1.day2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Exceptionhandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromeDriver.exe");
		ChromeDriver d=new ChromeDriver();	
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		d.get("http://leaftaps.com/opentaps");
		
		try {
			d.findElementById("username1").sendKeys("DemoSalesManager");
		} 
		catch (Error e) {
			System.out.println("No such element Exception occurred");
			throw new RuntimeErrorException(e);
			//e.printStackTrace();
		}
		finally
		{
			d.close();
		}
		d.findElementById("password").sendKeys("crmsfa");
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click();
		d.findElementByLinkText("Create Lead").click();
		//d.findElementById("createLeadForm_companyName").sendKeys("TCS");
		//d.findElementById("createLeadForm_firstName").sendKeys("Nivedha");
		//d.findElementById("createLeadForm_lastName").sendKeys("Harish");
		//d.findElementByName("submitButton").click();
		WebElement src1=d.findElementById("createLeadForm_dataSourceId");
		Select dd1=new Select(src1);
		dd1.selectByVisibleText("Direct Mail");
		//get value directly from drop down

		WebElement src2=d.findElementById("createLeadForm_marketingCampaignId");
		Select dd2=new Select(src2);
		List<WebElement> ls=dd2.getOptions();
		int size=ls.size();
		dd2.selectByIndex(size-2);
		//get last but one element using index size
		
		
	}

}
